﻿<!DOCTYPE html>
<html lang="en">
<head>
    <title>Acrogamers</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--link rel="shortcut icon" href="favicon.ico"-->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900' rel='stylesheet'
          type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,400italic,600italic,700italic'
          rel='stylesheet' type='text/css'>
    <!-- FontAwesome JS  GENERAL-->
    <script defer="" src="/web/assets\fontawesome\js\all.js"></script>
    <!-- Global CSS SOLO EN LANDING -->

    <link rel="stylesheet" href="/web/assets\plugins\bootstrap\css\bootstrap.min.css">
    <!-- Plugins CSS -->

    <!-- Theme CSS GENERAL -->
    <link id="theme-style" rel="stylesheet" href="/web/assets\css\theme-1.css">

    <!-- Particle GENERAL -->
    <link rel="stylesheet" href="/web/assets\css\particle.css">

    <!-- Brand GENERAL-->
    <link rel="stylesheet" href="/web/assets\css\branding.css">

</head>

<body>
<!-- ******HEADER****** -->
<header id="header" class="header fixed-top">
    <div class="container ">
        <nav class="main-nav navbar navbar-expand-md" role="navigation">
                 <span class="toggle-title">
                        <span class="brand-white"><span class="path1"></span><span class="path2"></span><span
                                class="path3"></span><span class="path4"></span><span class="path5"></span><span
                                class="path6"></span><span class="path7"></span><span class="path8"></span><span
                                class="path9"></span><span class="path10"></span><span class="path11"></span><span
                                class="path12"></span></span>

                    </span>
            <button class="navbar-toggler navbar-dark text-right " type="button" data-toggle="collapse"
                    data-target="#navbar-collapse" aria-controls="navbar-collapse" aria-expanded="false"
                    aria-label="Toggle navigation">

                <span class="navbar-toggler-icon"></span>
            </button>
            <div id="navbar-collapse" class="navbar-collapse collapse text-center justify-content-center">
                <ul class="nav navbar-nav">
                    <li class="nav-item"><a class="nav-link scrollto" href="#promo">INICIO</a></li>
                    <li class="nav-item"><a class="nav-link scrollto" href="#music">JUEGOS</a></li>
                    <li class="nav-item"><a class="nav-link scrollto" href="#gigs">CRONOGRAMA</a></li>
                    <li class="nav-item"><a class="nav-link scrollto" href="#about">RESULTADOS</a></li>
                    <li class="nav-item"><a class="nav-link scrollto" href="#merch">HIGHLIGHTS</a></li>
                    <li class="nav-item"><a class="nav-link scrollto" href="#contact">¡HOLA!</a></li>
                </ul><!--//nav-->
            </div><!--//navabr-collapse-->
        </nav><!--//main-nav-->
    </div><!--//container-->
</header><!--//header-->

<!-- ******Promo Section****** -->
<section id="promo" class="promo-section section">
    <div class="promo-carousel-holder" id="particles-js">
        <div id="promo-carousel" class="promo-carousel carousel slide carousel-fade" data-ride="carousel">
            <div class="carousel-inner carousel-zoom">
                <div class="carousel-item slide-1 active">
                    <div class="d-flex align-items-center justify-content-center min-vh-100">

                    </div>
                </div>

            </div>
        </div><!--//promo-carousel-->
    </div><!--//promo-carousel-holder-->
    <div class="overlay-mask"></div>
    <div class="container text-center promo-content">
        <div class="upper-wrapper">

            <h2 class="headline">EN TU MENTE,<br>EN TU JUEGO</h2>
            <div class="tagline">
                <a class="btn btn-secondary" href="#contact">REGISTRARSE</a>
            </div>
        </div>
    </div><!--//container-->

    <div class="updates-block">

        <div class="container updates-block-inner">
            <div id="carousel-updates" class="carousel slide" data-ride="carousel" data-interval="6000">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">

                    <div class="carousel-item active">

                        <div class="carousel-content no-media-holder">
                            <h3 class="title">HONOR CUP 2021</h3>
                            <div class="desc">
                                <div class="countdown">
                                    EMPIEZA EN
                                    <span id="countdown-box" class="countdown-box"></span>
                                </div><!--//countdown-->
                                <p class="intro">
                                    EL PRIMER TORNEO LATINOAMERICANO ORGANIZADO POR ACROGAMERS <a href="#honorCup"
                                                                                                  class="scrollto">Ver
                                        más</a>
                                </p><!--//intro-->
                            </div><!--//desc-->
                            <a class="btn btn-primary btn-cta" href="#">INSCRIBIRSE</a>
                        </div><!--//content-container-->
                    </div><!--//item-->

                    <div class="carousel-item">
                        <div class="carousel-content no-media-holder">
                            <h3 class="title">LA PLATAFORMA</h3>
                            <div class="desc">
                                <p class="intro">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                    dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                                    <br><a href="#" class="more-link" data-toggle="modal" data-target="#modal-info">Leer
                                        más</a>
                                </p><!--//intro-->
                            </div><!--//desc-->
                            <a class="btn btn-primary btn-cta" href="#">INICIAR SESION</a>
                        </div><!--//content-container-->
                    </div><!--//item-->

                    <div class="carousel-item">
                        <div class="carousel-content no-media-holder">
                            <h3 class="title">COMUNIDAD</h3>
                            <div class="desc">
                                <p class="intro">
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget
                                    dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes.
                                    <br><a href="#" class="more-link">Más información</a>
                                </p><!--//intro-->
                            </div><!--//desc-->
                            <a class="btn btn-primary btn-cta" href="#">IR A MI PERFIL</a>
                        </div><!--//content-container-->
                    </div><!--//item-->


                </div><!--//carousel-inner-->

                <!-- Controls -->
                <a class="carousel-control-prev" href="#carousel-updates" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel-updates" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div><!--//carousel-updates-->


        </div><!--//container-->
    </div><!--//updates-block-->

</section><!--//promo-section-->

<section id="music" class="music-section section text-center">
    <div class="container">
        <h2 class="section-title">Selecciona tu juego</h2>

        <div class="albums-block">
            <div class="row">


                <div class="item col-6 col-lg-4">
                    <div class="item-inner">
                        <h3 class="album-title">
                            COD MOBILE
                        </h3>
                        <div class="cover-holder">

                            <a class="cover-figure disabled">
                                <img class="cover-image img-fluid" src="/web/assets\images\albums\album-cover-2.jpg"
                                     alt="">
                                <div class="cover-label bg_grey">PRÓXIMAMENTE</div><!--//cover-label-->
                            </a><!--//cover-figure-->


                        </div><!--//cover-holder-->
                        <a href="#" class=" btn btn-sm btn-secondary disabled">NO DISPONIBLE</a>


                    </div><!--//item-inner-->
                </div><!--//item-->

                <div class="item col-6 col-lg-4">
                    <div class="item-inner">
                        <h3 class="album-title">
                            PUBG MOBILE
                        </h3>
                        <div class="cover-holder">

                            <a class="cover-figure" href="#">
                                <div class="arrow-holder"></div>
                                <img class="cover-image img-fluid" src="/web/assets\images\albums\album-cover-1.jpg"
                                     alt="">
                                <div class="cover-label">DISPONIBLE</div><!--//cover-label-->
                            </a><!--//cover-figure-->


                        </div><!--//cover-holder-->

                        <a href="#" class="btn btn-sm btn-ghost-secondary">SELECCIONAR</a>

                    </div><!--//item-inner-->
                </div><!--//item-->

                <div class="item col-6 col-lg-4">
                    <div class="item-inner">
                        <h3 class="album-title">
                            FREEFIRE
                        </h3>
                        <div class="cover-holder">

                            <a class="cover-figure">
                                <img class="cover-image img-fluid" src="/web/assets\images\albums\album-cover-3.jpg"
                                     alt="">
                                <div class="cover-label bg_grey">PRÓXIMAMENTE</div><!--//cover-label-->

                            </a><!--//cover-figure-->


                        </div><!--//cover-holder-->
                        <a href="#" class="btn btn-sm btn-secondary disabled">NO DISPONIBLE</a>


                    </div><!--//item-inner-->
                </div><!--//item-->

            </div><!--//row-->
        </div><!--//albums-block-->

    </div><!--//container-->

</section><!--//music-section-->
<section id="honorCup">
    <div>
        <img src="/web/assets/images/hero/honorCup.png" class="img-fluid" alt="">
    </div>
</section>
<section id="gigs" class="gigs-section section" style="background-color: black;">
    <div class="container">
        <h2 class="section-title text-center text-light">Cronograma</h2>
        <br>
        <span class="text-light">1º Semana | ENERO</span>
        <div class="gigs-container">
            <div class="item">
                <div class="date-label">
                    <div class="number">01</div>
                    <div class="month">ENE</div>
                </div><!--//date-label-->
                <div class="gig-info">
                    <div class="info-content">
                        <h3 class="gig-title text-light">SCRIMS ABIERTAS</h3>
                        <div class="meta">
                            <ul class="meta-list list-inline">
                                <li class="list-inline-item location">
                                    <i class="fas fa-map-marker-alt"></i><a href="#">ESTALINGRADO, SALA NORMAL</a>
                                </li>
                                <li class="list-inline-item time">
                                    <i class="far fa-clock"></i>9:00pm, Sábado
                                </li>
                            </ul>
                        </div><!--//meta-->
                    </div><!--//info-content-->
                    <div class="gig-actions">
                        <a class="btn btn-xs btn-secondary" href="#">PARTICIPAR</a>
                        <a class="btn btn-xs btn-ghost-secondary b-light" data-toggle="collapse" href="#info-extra-1"
                           aria-expanded="false" aria-controls="info-extra-1">RESULTADOS</a>

                    </div><!--//gig-actions-->
                    <div id="info-extra-1" class="collapse info-extra">
                        MUESTRA LA TABLA DE LOS RESULTADOS
                    </div>
                </div><!--//info-extra-->
            </div><!--//item-->
            <div class="item">
                <div class="date-label">
                    <div class="number">02</div>
                    <div class="month">ENE</div>
                </div><!--//date-label-->
                <div class="gig-info">
                    <div class="info-content">
                        <h3 class="gig-title text-light">SCRIMS SOCIOS</h3>
                        <div class="meta">
                            <ul class="meta-list list-inline">
                                <li class="list-inline-item  location">
                                    <i class="fas fa-map-marker-alt"></i><a href="#">ERANGEL, SALA AVANZADA</a>
                                </li>
                                <li class="list-inline-item  time">
                                    <i class="far fa-clock"></i>7:00pm, DOMINGO
                                </li>
                            </ul>
                        </div><!--//meta-->
                    </div><!--//info-content-->
                    <div class="gig-actions">
                        <a class="btn btn-xs btn-secondary" href="#">PARTICIPAR</a>
                        <a class="btn btn-xs btn-ghost-secondary b-light" data-toggle="collapse" href="#info-extra-2"
                           aria-expanded="false" aria-controls="info-extra-2">RESULTADOS</a>

                    </div><!--//gig-actions-->
                    <div id="info-extra-2" class="collapse info-extra">
                        MUESTRA LA TABLA
                    </div>
                </div><!--//info-extra-->
            </div><!--//item-->
            <div class="item">
                <div class="date-label">
                    <div class="number">03</div>
                    <div class="month">ENE</div>
                </div><!--//date-label-->
                <div class="gig-info">
                    <div class="info-content">
                        <h3 class="gig-title text-light">SCRIMS ENTRENAMIENTO</h3>
                        <div class="meta">
                            <ul class="meta-list list-inline">
                                <li class="list-inline-item  location">
                                    <i class="fas fa-map-marker-alt"></i><a href="#">ERANGEL, SALA NORMAL</a>
                                </li>
                                <li class="list-inline-item  time">
                                    <i class="far fa-clock"></i>8:30pm, LUNES
                                </li>
                            </ul>
                        </div><!--//meta-->
                    </div><!--//info-content-->
                    <div class="gig-actions">
                        <a class="btn btn-xs btn-secondary" href="#">PARTICIPAR</a>
                        <a class="btn btn-xs btn-ghost-secondary b-light" data-toggle="collapse" href="#info-extra-3"
                           aria-expanded="false" aria-controls="info-extra-3">RESULTADOS</a>
                    </div><!--//gig-actions-->
                    <div id="info-extra-3" class="collapse info-extra">
                        MUESTRA LA TABLA
                    </div>
                </div><!--//info-extra-->
            </div><!--//item-->

            <div class="item">
                <div class="date-label">
                    <div class="number">04</div>
                    <div class="month">ENE</div>
                </div><!--//date-label-->
                <div class="gig-info">
                    <div class="info-content">
                        <h3 class="gig-title text-light">SCRIMS MIXTAS</h3>
                        <div class="meta">
                            <ul class="meta-list list-inline">
                                <li class="list-inline-item location">
                                    <i class="fas fa-map-marker-alt"></i><a href="#">ERANGEL, SALA AVANZADA</a>
                                </li>
                                <li class="list-inline-item time">
                                    <i class="far fa-clock"></i>6:00pm, MARTES
                                </li>
                            </ul>
                        </div><!--//meta-->
                    </div><!--//info-content-->
                    <div class="gig-actions">
                        <a class="btn btn-xs btn-secondary" href="#">PARTICIPAR</a>
                        <a class="btn btn-xs btn-ghost-secondary b-light" data-toggle="collapse" href="#info-extra-4"
                           aria-expanded="false" aria-controls="info-extra-4">RESULTADOS</a>
                    </div><!--//gig-actions-->
                    <div id="info-extra-4" class="collapse info-extra">
                        MUESTRA LA TABLA DE RESULTADOS/ PUEDE SER IMAGEN TAMBIEN
                    </div>
                </div><!--//info-extra-->
            </div><!--//item-->
        </div><!--//gigs-container-->
        <div class="gigs-contact text-center text-light">
            <h4 class="title">QUIERO RESERVAR MI LUGAR</h4>
            <p class="intro text-center">Ponte en contacto con nuestro equipo de soporte al usuario</p>
            <a class="btn btn-ghost-primary scrollto" href="#contact">RESERVAR LUGAR</a>
        </div><!--//gigs-->
    </div><!--//container-->
</section><!--//gigs-section-->


<section id="about" class="about-section section text-center">
    <div class="container">
        <h2 class="section-title">LIGA ACROGAMERS</h2>
        <div class="section-intro ml-auto mr-auto">Resultados generales y top 3 de la liga acrogamers.</div>
        <div class="members-block">
            <div class="row">

                <div class="item col-6 col-lg-4">
                    <div class="item-inner">
                        <div class="member-profile">
                            <img class="img-fluid" src="/web/assets\images\members\member-2.png" alt="">
                        </div><!--//member-profile-->
                        <div class="member-label">2º do.</div>
                        <h3 class="member-name">
                            PUNTOS
                        </h3>
                        <div class="item">
                            <ul class="meta-list list-inline">
                                <li class="list-inline-item location">
                                    <i class="fas fa-skull"></i> KILLS: 15
                                </li>
                                <li class="list-inline-item location">
                                    <i class="fas fa-trophy"></i> KILLS: 15
                                </li>

                            </ul>

                        </div>


                    </div><!--//item-inner-->
                </div><!--//item-->
                <div class="item col-6 col-lg-4">
                    <div class="item-inner">
                        <div class="member-profile">
                            <img class="img-fluid" src="/web/assets\images\members\member-3.png" alt="">

                        </div><!--//member-profile-->
                        <div class="member-label">1º er.</div>
                        <h3 class="member-name">
                            PUNTOS
                        </h3>
                        <div class="item">
                            <ul class="meta-list list-inline">
                                <li class="list-inline-item location">
                                    <i class="fas fa-skull"></i> KILLS: 15
                                </li>
                                <li class="list-inline-item location">
                                    <i class="fas fa-trophy"></i> KILLS: 15
                                </li>

                            </ul>

                        </div>

                    </div><!--//item-inner-->
                </div><!--//item-->
                <div class="item col-6 col-lg-4">
                    <div class="item-inner">
                        <div class="member-profile">
                            <img class="img-fluid" src="/web/assets\images\members\member-1.png" alt="">

                        </div><!--//member-profile-->
                        <div class="member-label">3º ero.</div>
                        <h3 class="member-name">
                            PUNTOS
                        </h3>
                        <div class="item">
                            <ul class="meta-list list-inline">
                                <li class="list-inline-item location">
                                    <i class="fas fa-skull"></i> KILLS: 15
                                </li>
                                <li class="list-inline-item location">
                                    <i class="fas fa-trophy"></i> KILLS: 15
                                </li>

                            </ul>

                        </div>

                    </div><!--//item-inner-->
                </div><!--//item-->
            </div><!--//row-->
        </div><!--//members-block-->
    </div><!--//container-->
</section><!--//about-section-->

<section id="merch" class="merch-section section text-center">
    <div class="container">
        <h2 class="section-title">Highlights</h2>
        <div class="row">
            <div class="item col-12 col-md-6">
                <div class="item-inner">
                    <img class="img-fluid" src="/web/assets\images\merch\merch-1.jpg" alt="">
                    <a class="mask" href="#">
                            <span class="mask-inner">
                                <span class="item-title">Fusilero</span>
                                <!--span class="desc">.</span-->
                                <span style="font-size: xx-large; margin: 70px;"><i class="fas fa-play"></i></span>
                            </span>
                    </a>
                </div><!--//item-inner-->
            </div>
            <div class="item col-12 col-md-6">
                <div class="item-inner">
                    <img class="img-fluid" src="/web/assets\images\merch\merch-2.jpg" alt="">
                    <a class="mask" href="#">
                            <span class="mask-inner">
                                <span class="item-title">Francotirador</span>
                                <span style="font-size: xx-large; margin: 70px;"><i class="fas fa-play"></i></span>
                            </span>
                    </a>
                </div><!--//item-inner-->
            </div>
            <div class="item col-12 col-md-6">
                <div class="item-inner">
                    <img class="img-fluid" src="/web/assets\images\merch\merch-3.jpg" alt="">
                    <a class="mask" href="#">
                            <span class="mask-inner">
                                <span class="item-title">La sartén</span>
                                <span style="font-size: xx-large; margin: 70px;"><i class="fas fa-play"></i></span>
                            </span>
                    </a>
                </div>
            </div>
            <div class="item col-12 col-md-6">
                <div class="item-inner">
                    <img class="img-fluid" src="/web/assets\images\merch\merch-4.jpg" alt="">
                    <a class="mask" href="#">
                            <span class="mask-inner">
                                <span class="item-title">Equipo</span>
                                <span style="font-size: xx-large; margin: 70px;"><i class="fas fa-play"></i></span>
                            </span>
                    </a>
                </div>
            </div>
        </div><!--//row-->

        <div class="merch-action">
            <a class="btn btn-ghost-primary" href="#">Ver todo</a>
        </div>

    </div><!--//container-->

</section><!--//merch-section-->

<div id="contact" class="contact-section section text-center">
    <div class="container">
        <h2 class="section-title">INFORMACIÓN DE CONTACTO</h2>
        <div class="section-intro center-block">Tenes alguna duda con respecto a proximos lanzamientos o....</div>
        <div class="contact-block mr-auto ml-auto">
            <div class="row">

                <div class="item col-12 col-lg-4">
                    <div class="item-inner">
                        <div class="icon-holder">
                            <i class="fas fa-info-circle"></i>
                        </div><!--//icon-holder-->
                        <h4 class="title">EMAIL</h4>
                        <div class="email"><a href="#">info@acrogamers.com</a></div>
                    </div><!--//item-inner-->
                </div><!--//item-->

                <div class="item col-12 col-lg-4">
                    <div class="item-inner">
                        <div class="icon-holder">
                            <i class="fab fa-whatsapp"></i>
                        </div><!--//icon-holder-->
                        <h4 class="title">WHATSAPP</h4>
                        <div class="email"><a href="#">+595 9xx xxx xxx</a></div>
                    </div><!--//item-inner-->
                </div><!--//item-->

                <div class="item col-12 col-lg-4">
                    <div class="item-inner">
                        <div class="icon-holder">
                            <i class="far fa-question-circle"></i>
                        </div><!--//icon-holder-->
                        <h4 class="title">FAQs</h4>
                        <div class="email"><a href="#">Preguntas Frecuentes</a></div>
                    </div><!--//item-inner-->
                </div><!--//item-->
            </div><!--//row-->
        </div><!--//contact-list-->

        <div class="social-media-block">
            <ul class="list-inline social-media-list">
                <li class="list-inline-item mr-3"><a href="#"><i class="fab fa-youtube"></i></a></li>
                <li class="list-inline-item mr-3"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
                <li class="list-inline-item mr-3"><a href="#"><i class="fab fa-instagram"></i></a></li>
                <li class="list-inline-item mr-3"><a href="#"><i class="fab fa-twitch"></i></a></li>


            </ul>
        </div><!--//social-media-block-->
    </div><!--//containter-->
</div>

<!-- ******FOOTER****** -->
<footer class="footer">
    <div class="container">
        <div class="footer-content text-center">

            <div class="copyright">
                LATINOAMERICA
                <br>
                TODOS LOS DERECHOS RESERVADOS Y PROPIEDAD INTELECTUAL<br>
                <br>
                <h3><span class="brand-white"><span class="path1"></span><span class="path2"></span><span
                            class="path3"></span><span class="path4"></span><span class="path5"></span><span
                            class="path6"></span><span class="path7"></span><span class="path8"></span><span
                            class="path9"></span><span class="path10"></span><span class="path11"></span><span
                            class="path12"></span></span>
                </h3>
            </div>
        </div><!--//footer-content-->
    </div><!--//container-->
</footer><!--//footer-->


<!-- Video Modal -->
<div class="modal modal-video" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="videoModalLabel"
     aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 id="videoModalLabel" class="modal-title sr-only">Music Video</h4>
            </div>
            <div class="modal-body">
                <div class="video-container">
                    <div class="ratio ratio-16x9">
                        <iframe src="https://www.youtube.com/embed/mCHUw7ACS8o?rel=0" title="YouTube video"
                                allowfullscreen=""></iframe>
                    </div>
                </div><!--//video-container-->
            </div><!--//modal-body-->
        </div><!--//modal-content-->
    </div><!--//modal-dialog-->
</div><!--//modal-->

<!-- Info Modal -->
<div id="modal-info" class="modal-info modal" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title text-center" id="infoModalLabel">We are performing at The Fleece, Bristol</h4>
            </div><!--//modal-header-->
            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean
                    massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec
                    quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.
                    Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim <a href="#">mollis
                        pretium</a> justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede.
                </p>
                <div class="center-block text-center">
                    <iframe
                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2486.319931873737!2d-2.59156458423205!3d51.45228217962615!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48718e7bd5fd30fd%3A0x9acbbf4d84e5ca8c!2sThe+Fleece!5e0!3m2!1sen!2sus!4v1454805283508"
                        width="640" height="360" frameborder="0" style="border:0" allowfullscreen=""></iframe>
                </div>
            </div><!--//modal-body-->
        </div><!--//modal-content-->
    </div>
</div><!--//modal-->


<!-- Main Javascript GENRAL -->
<script src="/web/assets\plugins\popper.min.js"></script>
<!-- Main Javascript SOLO EN LANDING -->
<script src="/web/assets\plugins\bootstrap\js\bootstrap.min.js"></script>
<!-- Main Javascript GENRAL -->
<script src="/web/assets\plugins\vanilla-back-to-top.min.js"></script>
<script src="/web/assets\plugins\smoothscroll.min.js"></script>
<script src="/web/assets\plugins\gumshoe\gumshoe.polyfills.min.js"></script>
<script src="/web/assets\js\main.js"></script>


<!-- JS PARTICLE GENERAL-->
<script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
<script src="/web/assets\js\config_particle.js"></script>


</body>
</html>

