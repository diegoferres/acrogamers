<!DOCTYPE html>
<html lang="en">
<head>
    <title>Acrogamers</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--link rel="shortcut icon" href="favicon.ico"-->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900' rel='stylesheet'
          type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,400italic,600italic,700italic'
          rel='stylesheet' type='text/css'>
    <!-- FontAwesome JS -->
    <script defer="" src="/web/assets\fontawesome\js\all.js"></script>
    <!-- Global CSS -->
    <link rel="stylesheet" href="/web/assets\plugins\bootstrap\css\bootstrap.min.css">
    <!-- Plugins CSS -->

    <!-- Theme CSS -->
    <link id="theme-style" rel="stylesheet" href="/web/assets\css\theme-1.css">

    <!-- Particle -->
    <link rel="stylesheet" href="/web/assets\css\particle.css">

    <!-- Brand -->
    <link rel="stylesheet" href="/web/assets\css\branding.css">


    {{-- intlTelInput --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/css/intlTelInput.css">

    {{-- COUNTRYPICKER--}}
    <link rel="stylesheet"
          href="/web/js/countrypicker/dependancies/bootstrap-select-1.12.4/dist/css/bootstrap-select.min.css">
    <style>

        footer {

            position: absolute;
            bottom: -400px;
            width: 100%;
            height: 60px;
            text-align: center;

        }

        .dropdown-toggle.btn-default {
            color: #fff;
            background-color: #000000;
            border-color: #ccc;
        }

        textarea {
            background-color: transparent;
            border: 1px solid #fff;
        }

        textarea:focus {
            outline: none !important;
            border-color: #719ECE;
            box-shadow: 0 0 10px #719ECE;
        }

        .form-control {
            display: block;
            width: 100%;
            padding: .375rem .75rem;
            font-size: 1rem;
            line-height: 1.5;
            color: #495057;
            background-color: transparent;
            background-clip: padding-box;
            border: 1px solid #ced4da;
            border-radius: .25rem;
            transition: border-color .15s ease-in-out, box-shadow .15s ease-in-out;
        }

        .bootstrap-select.show > .dropdown-menu > .dropdown-menu {
            display: block;
            width: 100% !important;
        }

        .bootstrap-select > .dropdown-menu > .dropdown-menu li.hidden {
            display: none;
        }

        .bootstrap-select > .dropdown-menu > .dropdown-menu li a {
            display: block;
            width: 100%;
            padding: 3px 1.5rem;
            clear: both;
            font-weight: 400;
            color: #292b2c;
            text-align: inherit;
            white-space: nowrap;
            background: 0 0;
            border: 0;
            text-decoration: none;
        }

        .bootstrap-select > .dropdown-menu > .dropdown-menu li a:hover {
            background-color: gray;
        }

        .bootstrap-select > .dropdown-toggle {
            width: 100%;
        }

        .dropdown-menu > li.active > a {
            color: #000000 !important;
            background-color: #337ab7 !important;
        }

        .bootstrap-select .check-mark {
            line-height: 14px;
        }

        .bootstrap-select .check-mark::after {
            font-family: "FontAwesome";
            content: "\f00c";
        }

        .bootstrap-select button {
            overflow: hidden;
            text-overflow: ellipsis;
        }

        /* Make filled out selects be the same size as empty selects */
        .bootstrap-select.btn-group .dropdown-toggle .filter-option {
            display: inline !important;
        }

        .dropdown-menu {
            position: absolute;
            top: 100%;
            left: 0;
            z-index: 1000;
            display: none;
            float: left;
            min-width: 10rem;
            padding: .5rem 0;
            margin: .125rem 0 0;
            font-size: 1rem;
            color: #FFFFFF;
            text-align: left;
            list-style: none;
            background-color: #000000;
            background-clip: padding-box;
            border: 1px white !important;
            border-radius: .25rem;
        }

        .text {
            color: white !important;
        }

        .btn-group.bootstrap-select.countrypicker {
            width: 100% !important;
        }

        .dropdown-menu.open.show {
            width: 100% !important;
        }

        .bootstrap-select:not([class*=col-]):not([class*=form-control]):not(.input-group-btn) {
            width: 100% !important;
        }

        /*
        CROPIT
         */
        .cropit-preview {
            background-color: #f8f8f8;
            background-size: cover;
            border: 5px solid #ccc;
            border-radius: 3px;
            margin-top: 7px;
            width: 250px;
            height: 250px;
        }

        .cropit-preview-image-container {
            cursor: move;
        }

        .cropit-preview-background {
            opacity: .2;
            cursor: auto;
        }

        .image-size-label {
            margin-top: 10px;
        }

        input, .export {
            /* Use relative position to prevent from being covered by image background */
            position: relative;
            z-index: 10;
            display: block;
        }

        button {
            margin-top: 10px;
        }

        /*
        END CROPIT
         */

        /*
       Breadcrumb
        */
        ul.breadcrumb {
            padding: 10px 16px;
            list-style: none;
            background-color: #000000;

        }

        ul.breadcrumb li {
            display: inline;
            font-size: 18px;

        }

        ul.breadcrumb li + li:before {
            padding: 8px;
            color: white;
            content: "/\00a0";
        }

        ul.breadcrumb li a {
            color: #0275d8;
            text-decoration: none;
            color: white !important;
        }

        ul.breadcrumb li a:hover {
            color: #01447e;
            text-decoration: underline;
        }

        /*
        End breadcrumb
         */

    </style>
</head>

<body>

<section id="gigs" class="gigs-section section" style="background-color: black;">
    <div class="container">
        <h2 class="section-title text-center text-light">Crea una cuenta</h2>
        <br>
        <form action="">

            <label>Nombre y Apellido:</label>
            <input class="form-control" type="text">
            <label>Correo:</label>
            <input class="form-control" type="text">


            <label>Teléfono:</label>
            <input type="tel" placeholder=""
                   class="form-control is-invalid" id="phone" name="phone"
                   value="{{ old('full_number') }}" required>


            <label>País:</label>
            <div>
                {!! Form::select('country', [] , null , ['class' => 'selectpicker countrypicker is-invalid',
                                                                                     'placeholder' => 'País',
                                                                                     'id' => 'country',
                                                                                     'data-flag' => 'true',
                                                                                     'data-live-search'=>'true']) !!}
                {!! Form::hidden('country_id', '', ['id' => 'country_id']) !!}
            </div>


            <label>Contraseña:</label>
            <input class="form-control" type="password">
            <label>Confirmar contraseña:</label>
            <input class="form-control" type="password">
            <br>
            <a class="btn btn-block btn-ghost-primary scrollto" data-toggle="collapse" href="#info-extra-1"
               aria-expanded="false" aria-controls="info-extra-1">Registrarme</a>

            <label class="text-light" for="">Ya tengo una cuenta.<a href="#"> Inicia sesión</a></label>
            <hr>
            <h3 class="gig-title text-light">Registrate con tus redes sociales</h3>
            <a href="#" class="btn btn-facebook"><i class="fab fa-facebook-f"></i></a>
            <a href="#" class="btn btn-google"><i class="fab fa-google"></i></a>


        </form>


    </div><!--//container-->
</section><!--//gigs-section-->
<!-- Main Javascript -->
<script src="/web/assets\plugins\popper.min.js"></script>
<script src="/web/assets\plugins\bootstrap\js\bootstrap.min.js"></script>
<script src="/web/assets\plugins\vanilla-back-to-top.min.js"></script>
<script src="/web/assets\plugins\smoothscroll.min.js"></script>
<script src="/web/assets\plugins\gumshoe\gumshoe.polyfills.min.js"></script>
<script src="/web/assets\js\main.js"></script>

<!-- JS PARTICLE-->
<script src="https://cdn.jsdelivr.net/particles.js/2.0.0/particles.min.js"></script>
<script src="/web/assets\js\config_particle.js"></script>

{{-- JQUERY --}}
<script
    src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.js"
></script>

{{-- intlTelInput --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/intlTelInput-jquery.js"></script>

{{-- Countrypicker --}}
<script src="/web/js/countrypicker/dependancies/bootstrap-select-1.12.4/dist/js/bootstrap-select.min.js"></script>
<script src="/web/js/countrypicker/countrypicker.min.js"></script>

<script>
    /*
    intlTelInput & countrypicker
     */
    $(document).ready(function () {
        $('#country').change(function () {
            $('#country_id').val($('option:selected').attr('data-country-code'))
        });

        $('[name="full_number"]').val(iti.getNumber())
    });

    var input = document.querySelector("#phone");
    var iti = window.intlTelInput(input, {
        allowDropdown: true,
        autoHideDialCode: false,
        //autoPlaceholder: "aggressive",
        dropdownContainer: document.body,
        // excludeCountries: ["us"],
        formatOnDisplay: true,
        geoIpLookup: function (callback) {
            jQuery.get("https://ipinfo.io", function () {
            }, "jsonp").always(function (resp) {
                var countryCode = (resp && resp.country) ? resp.country : "";
                callback(countryCode);


                $('#country_id').val(countryCode);

                $('#country').val($("option[data-country-code='" + countryCode + "']").val());
                $('#country').selectpicker('refresh');
            });
        },
        hiddenInput: "full_number",
        initialCountry: "auto",
        // localizedCountries: { 'de': 'Deutschland' },
        nationalMode: true,
        // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
        placeholderNumberType: "MOBILE",
        // preferredCountries: ['cn', 'jp'],
        separateDialCode: false,
        utilsScript: "https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/17.0.8/js/utils.js",
    });

    var handleChange = function () {
        reset();
        iti.setNumber(iti.getNumber())
        $('[name="full_number"]').val(iti.getNumber())
        if (input.value.trim()) {
            if (iti.isValidNumber()) {
                input.classList.add("is-valid");
            } else {
                input.classList.add("is-invalid");
            }
        }
    };

    var reset = function () {
        input.classList.remove("is-invalid");
    };

    // listen to "keyup", but also "change" to update when the user selects a country
    input.addEventListener('change', handleChange);
    input.addEventListener('keyup', handleChange);
    /*
    END intlTelInput & countrypicker
     */

</script>
</body>


</html>
